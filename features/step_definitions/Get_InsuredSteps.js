/**
 * Created by subhajit-chakraborty on 7/6/2017.
 */

var data = require('../../TestResources/Get_InsuredGlobalData.js');
var BrowserData = require('../../TestResources/GlobalTestData.js');
var Objects = require(__dirname + '/../../repository/Get_InsuredPages.js');

var GetInsuredPage,LandingPageGI,ContactUsPage,MedicalIDPage,HIMPage,SocialSecurityPage,FAQPage,ShopForHealthInsurancePage,SubsidyAccountPage;

function initializePageObjects(browser, callback) {
    var GetInsuredPage = browser.page.Get_InsuredPages();
    LandingPageGI = GetInsuredPage.section.LandingPage;
    ContactUsPage = GetInsuredPage.section.ContactUsPage;
    MedicalIDPage = GetInsuredPage.section.MedicalIDPage;
    HIMPage = GetInsuredPage.section.HIMPage;
    SocialSecurityPage = GetInsuredPage.section.SocialSecurityPage;
    FAQPage = GetInsuredPage.section.FAQPage;
    ShopForHealthInsurancePage = GetInsuredPage.section.ShopForHealthInsurancePage;
    SubsidyAccountPage = GetInsuredPage.section.SubsidyAccountPage;
    callback();
}

module.exports = function() {

    this.Given(/^User opens the getInsured URl "([^"]*)"$/, function (GI_URL) {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                console.log(GI_URL);
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(GI_URL);
                browser.timeoutsImplicitWait(30000);
                LandingPageGI.waitForElementVisible('@TollFreeNumberText',data.LoginWait)
                    .waitForElementVisible('@HomeLink',data.LoginWait)
                    .waitForElementVisible('@ContactUsLink',data.LoginWait)
                    .waitForElementVisible('@HelpfulLinks',data.LoginWait)
                    //.waitForElementVisible('@SubsidyAccount',data.LoginWait)
                    // .waitForElementVisible('@SignUpLink',data.LoginWait)
                    .waitForElementVisible('@LogInLink',data.LoginWait)
                // action.elementDisplayedStatus("LandingPage|GetStartedButton", function (status) {
                //     if (status == true) {
                //         LandingPageGI.waitForElementVisible('@GetStartedButton',data.LoginWait)
                //     }
                //     else
                //     {
                //         console.log("ABC");
                //     }
                // });


            });
        }
    });

    this.When(/^User able to verify the tollfree number "([^"]*)"$/, function (TollNumber) {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                browser.useXpath().assert.containsText("//a[@id='ClickTrackIVRNumberNull']/../a[@id='ClickTrackNumberNotNull']",TollNumber);
            });
        }
    });

    this.When(/^User able to Verify Contact Us header of that page$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                LandingPageGI.waitForElementVisible('@ContactUsLink',data.LoginWait)
                    .click('@ContactUsLink');

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.longWait)
                });
                ContactUsPage.waitForElementVisible('@ContactUsMarketplaceHeader',data.LoginWait);
                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                LandingPageGI.waitForElementVisible('@ContactUsLink',data.LoginWait)
            });
        }
    });

    this.When(/^User able to navigate to hompage after clicking Home Button$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                LandingPageGI.waitForElementVisible('@HomeLink',data.LoginWait)
                    .click('@HomeLink');
                LandingPageGI.waitForElementVisible('@HomeLink',data.LoginWait)
                    .waitForElementVisible('@ContactUsLink',data.LoginWait)
                    .waitForElementVisible('@HelpfulLinks',data.LoginWait)
                    .waitForElementVisible('@HRARetireeAccount',data.LoginWait)
            });
        }
    });

    this.When(/^User able to validate help link headers of Landing Pages$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {

                //Medical ID Link Verification
                LandingPageGI.waitForElementVisible('@HelpfulLinks',data.LoginWait)
                    .click('@HelpfulLinks')
                    .waitForElementVisible('@MedicaIDLink',data.LoginWait)
                    .click('@MedicaIDLink');

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                //MedicalIDPage.waitForElementVisible('@MedicalIDImg',data.LoginWait);
                browser.assert.urlContains('https://www.medicaid.gov/');
                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                LandingPageGI.waitForElementVisible('@ContactUsLink',data.LoginWait)
                    .waitForElementVisible('@HelpfulLinks',data.LoginWait);


                //Health Insurance Marketplace Verification
                LandingPageGI.waitForElementVisible('@HelpfulLinks',data.LoginWait)
                    .click('@HelpfulLinks')
                    .waitForElementVisible('@HIMLink',data.LoginWait)
                    .click('@HIMLink');

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                //HIMPage.waitForElementVisible('@HealthInsuranceHeader',data.LoginWait);
                browser.assert.urlContains('//www.healthcare.gov/');
                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                LandingPageGI.waitForElementVisible('@ContactUsLink',data.LoginWait)
                    .waitForElementVisible('@HelpfulLinks',data.LoginWait);


                //Social Security Verification
                LandingPageGI.waitForElementVisible('@HelpfulLinks',data.LoginWait)
                    .click('@HelpfulLinks')
                    .waitForElementVisible('@SocialSecurityLink',data.LoginWait)
                    .click('@SocialSecurityLink');

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                // SocialSecurityPage.waitForElementVisible('@SocialSecurityHeader',data.LoginWait);
                browser.assert.urlContains('https://www.ssa.gov/');

                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                LandingPageGI.waitForElementVisible('@ContactUsLink',data.LoginWait)
                    .waitForElementVisible('@HelpfulLinks',data.LoginWait);


                // FAQ Page Verification
                LandingPageGI.waitForElementVisible('@HelpfulLinks',data.LoginWait)
                    .click('@HelpfulLinks')
                    .waitForElementVisible('@FAQsLink',data.LoginWait)
                    .click('@FAQsLink');

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                //HIMPage.waitForElementVisible('@HealthInsuranceHeader',data.LoginWait);
                browser.assert.urlContains('https://www.healthcare.gov/get-answers/');
                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                LandingPageGI.waitForElementVisible('@ContactUsLink',data.LoginWait)
                    .waitForElementVisible('@HelpfulLinks',data.LoginWait);

            });
        }
    });

    this.When(/^User should able to verify the login page of "([^"]*)" Portals$/, function (Test_URL) {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                if(Test_URL.includes("subsidy"))
                {
                    LandingPageGI.waitForElementVisible('@GetStartedLoginGatedPortal',data.LoginWait)
                        .click('@GetStartedLoginGatedPortal')
                        .waitForElementVisible('@FirstnameTextbox',data.LoginWait)
                        .waitForElementVisible('@LastnameTextbox',data.LoginWait)
                        .waitForElementVisible('@SSNTextbox',data.LoginWait)
                        .waitForElementVisible('@HomeLink',data.LoginWait)
                        .click('@HomeLink')
                        .waitForElementVisible('@GetStartedLoginGatedPortal',data.LoginWait);
                }
                else if(Test_URL.includes("accessonly"))
                {
                    LandingPageGI.waitForElementVisible('@StartShoppingLoginUngatedPortal',data.LoginWait)
                        .click('@StartShoppingLoginUngatedPortal')
                        .waitForElementVisible('@ShoppingHeaderUnGatedPortal',data.LoginWait)
                        .waitForElementVisible('@HomeLink',data.LoginWait)
                        .click('@HomeLink')
                        .waitForElementVisible('@StartShoppingLoginUngatedPortal',data.LoginWait);
                }
            });
        }
    });

    this.When(/^User should able to verify the footer Paragraphs$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                LandingPageGI.waitForElementPresent('@FooterParaLink1',data.LoginWait)
                    .waitForElementPresent('@FooterParaLink2',data.LoginWait);
                browser.getText("p[footer-disclaimer-links='0']", function(result) {
                    this.assert.equal(typeof result, "object");
                    this.assert.equal(result.status, 0);
                    this.assert.equal(result.value, data.FooterPara1);
                });

                browser.getText("p[footer-disclaimer-links='1']", function(result) {
                    this.assert.equal(typeof result, "object");
                    this.assert.equal(result.status, 0);
                    this.assert.equal(result.value, data.FooterPara2);
                });
            });
        }
    });

    this.When(/^User able to verify the logo of Mercer Marketplace$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                LandingPageGI.waitForElementPresent('@MercerImgLink',data.LoginWait)
            });
        }
    });

    this.Then(/^User should able to verify terms and Condition and Privacy Policy link$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                LandingPageGI.waitForElementPresent('@PrivacyPolicyLink',data.LoginWait)
                    .click('@PrivacyPolicyLink')
                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });
                browser.assert.urlContains('http://retiree.mercermarketplace.com/privacy-policy.html');
                browser.closeWindow();

                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });
                LandingPageGI.waitForElementPresent('@TermsAndConditionLink',data.LoginWait)
                    .click('@TermsAndConditionLink')
                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });
                browser.assert.urlContains('http://retiree.mercermarketplace.com/terms-and-conditions.html');
                browser.closeWindow();

                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });
            });
        }
    });

    this.When(/^User clicks on Get Started Link and login with "([^"]*)" "([^"]*)" and "([^"]*)"$/, function (First_Name,Last_Name,SSN) {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                LandingPageGI.waitForElementVisible('@GetStartedLoginGatedPortal',data.LoginWait)
                    .click('@GetStartedLoginGatedPortal')
                    .waitForElementVisible('@FirstnameTextbox',data.LoginWait)
                    .waitForElementVisible('@LastnameTextbox',data.LoginWait)
                    .waitForElementVisible('@SSNTextbox',data.LoginWait)
                    .setValue('@FirstnameTextbox',First_Name)
                    .setValue('@LastnameTextbox',Last_Name)
                    .setValue('@SSNTextbox',SSN)
                    .waitForElementVisible('@LoginContinueButton',data.LoginWait)
                    .click('@LoginContinueButton')
            });
        }
    });

    this.When(/^User able to login Successfully in Gated portal and Shop for Header Insurance Page should be displayed$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementPresent('@HealthInsHeader',data.LoginWait);
            });
        }
    });

    this.When(/^User able to Complete the Off Exchange flow for gated portal$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementPresent('@LostCoverageLink',data.LoginWait)
                    .click('@LostCoverageLink')
                    .waitForElementVisible('@SeeYourOptionsLink',data.LoginWait)
                    .click('@SeeYourOptionsLink')
                    .waitForElementVisible('@CompareYourOptionTitle',data.LoginWait)
                    .waitForElementVisible('@ShopWithEmpContButton',data.LoginWait)
                    .click('@ShopWithEmpContButton');

                //Verify Off-Exchange Url
                browser.pause(data.shortWait);
                browser.assert.urlContains('exchangeType=OFF');




            });
        }
    });

    this.When(/^User able to complete the On Exchange flow for Gated Portal$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementPresent('@LostCoverageLink',data.LoginWait)
                    .click('@LostCoverageLink')
                    .waitForElementVisible('@SeeYourOptionsLink',data.LoginWait)
                    .click('@SeeYourOptionsLink')
                    .waitForElementVisible('@CompareYourOptionTitle',data.LoginWait)
                    .waitForElementVisible('@ShopWithTaxCreditsButton',data.LoginWait)
                    .click('@ShopWithTaxCreditsButton');

                //Verify Off-Exchange Url
                browser.pause(data.shortWait);
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });
                browser.assert.urlContains('exchangeType=ON');

                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });
                ShopForHealthInsurancePage.waitForElementVisible('@CompareYourOptionTitle',data.LoginWait);
            });
        }
    });

    this.When(/^User able to login Successfully in Non-Gated portal and Shop for Header Insurance Page should be displayed$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {

                LandingPageGI.waitForElementVisible('@StartShoppingLoginUngatedPortal',data.LoginWait)
                    .click('@StartShoppingLoginUngatedPortal');
            });
        }
    });

    this.Then(/^User able to Complete the Off Exchange flow for Non-Gated portal$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {

                ShopForHealthInsurancePage.waitForElementVisible('@HealthInsHeader',data.LoginWait)
                    .waitForElementVisible('@LostCoverageLink',data.LoginWait)
                    .click('@LostCoverageLink')

                    //change Zipcode
                    .waitForElementVisible('@ZipcodeTextbox',data.LoginWait)
                    .clearValue('@ZipcodeTextbox')
                    .setValue('@ZipcodeTextbox',data.Zipcode)

                    //change Month
                    .waitForElementVisible('@MonthTextbox',data.LoginWait)
                    .clearValue('@MonthTextbox')
                    .setValue('@MonthTextbox',data.Month)

                    //change Day
                    .waitForElementVisible('@DaysTextbox',data.LoginWait)
                    .clearValue('@DaysTextbox')
                    .setValue('@DaysTextbox',data.Day)

                    //change Year
                    .waitForElementVisible('@YearTextbox',data.LoginWait)
                    .clearValue('@YearTextbox')
                    .setValue('@YearTextbox',data.Year)

                    .waitForElementVisible('@SeePlansLink',data.LoginWait)
                    .click('@SeePlansLink')

                browser.pause(data.shortWait)
                browser.assert.urlContains('exchangeType=OFF');
            });
        }
    });

    this.Then(/^User able to Complete the On Exchange flow for Non-Gated portal$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {

                ShopForHealthInsurancePage.waitForElementVisible('@HealthInsHeader',data.LoginWait)
                    .waitForElementVisible('@LostCoverageLink',data.LoginWait)
                    .click('@LostCoverageLink')

                    //change Zipcode
                    .waitForElementVisible('@ZipcodeTextbox',data.LoginWait)
                    .clearValue('@ZipcodeTextbox')
                    .setValue('@ZipcodeTextbox',data.Zipcode)

                    //change Month
                    .waitForElementVisible('@MonthTextbox',data.LoginWait)
                    .clearValue('@MonthTextbox')
                    .setValue('@MonthTextbox',data.Month)

                    //change Day
                    .waitForElementVisible('@DaysTextbox',data.LoginWait)
                    .clearValue('@DaysTextbox')
                    .setValue('@DaysTextbox',data.Day)

                    //change Year
                    .waitForElementVisible('@YearTextbox',data.LoginWait)
                    .clearValue('@YearTextbox')
                    .setValue('@YearTextbox',data.Year)

                    .waitForElementVisible('@CheckForTaxCreditsButton',data.LoginWait)
                    .click('@CheckForTaxCreditsButton')

                    .waitForElementVisible('@HouseholdIncomeTextbox',data.LoginWait)
                    .clearValue('@HouseholdIncomeTextbox')
                    .setValue('@HouseholdIncomeTextbox',data.HouseholdIncomeValue)

                    .waitForElementVisible('@CheckForTaxCreditsButtonNonGated',data.LoginWait)
                    .click('@CheckForTaxCreditsButtonNonGated')

                    .waitForElementVisible('@ShopWithTaxCreditsButton',data.LoginWait)
                    .click('@ShopWithTaxCreditsButton')

                browser.pause(data.shortWait)
                browser.assert.urlContains('exchangeType=ON');
            });
        }
    });

    this.When(/^User able to click and verify Health Product Link for Gated Portal$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementPresent('@HealthProductLink',data.LoginWait)
                    .click('@HealthProductLink');
                LandingPageGI.waitForElementVisible('@GetStartedButton',data.LoginWait);
            });
        }
    });

    this.When(/^User able to click and verify Medicare Product Link for Gated Portal$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementPresent('@MedicareProductLink',data.LoginWait)
                    .click('@MedicareProductLink')

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                browser.assert.urlContains('https://mercermarketplace.destinationrx.com/PlanCompare/Consumer/type1/2017/Compare/PinHome');
                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

            });
        }
    });

    this.When(/^User able to click and verify Dental Product Link for Gated Portal$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementPresent('@DentalProductLink',data.LoginWait)
                    .click('@DentalProductLink')

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                browser.assert.urlContains('dental');
                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

            });
        }
    });

    this.When(/^User able to click and verify Vision Product Link for Gated Portal$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementPresent('@VisionProductLink',data.LoginWait)
                    .click('@VisionProductLink')

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                browser.assert.urlContains('vision');
                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

            });
        }
    });

    this.When(/^User able to verify Other Insurance Product link in Motorola Clients "([^"]*)" for Gated Portal$/, function (URL_Moto) {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {

                if(URL_Moto.includes("Motorola"))
                {
                    ShopForHealthInsurancePage.waitForElementPresent('@OtherInsuranceLink',data.LoginWait)
                        .click('@OtherInsuranceLink')

                    browser.pause(data.shortWait)
                    browser.windowHandles(function(result) {
                        var handle = result.value[1];
                        browser.switchWindow(handle);
                        browser.maximizeWindow();
                        browser.pause(data.shortWait)
                    });

                    browser.assert.urlContains('Other-Insurance');
                    browser.closeWindow();

                    browser.pause(data.shortWait)
                    browser.windowHandles(function(result) {
                        var handle = result.value[0];
                        browser.switchWindow(handle);
                        browser.maximizeWindow();
                        browser.pause(data.shortWait)
                    });
                }

            });
        }
    });

    this.When(/^User able to click and verify Health Product Link for Non-Gated Portal$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementPresent('@HealthProductLink',data.LoginWait)
                    .click('@HealthProductLink');
                LandingPageGI.waitForElementVisible('@StartShoppingLoginUngatedPortal',data.LoginWait);
            });
        }
    });

    this.When(/^User able to click and verify Medicare Product Link for Non-Gated Portal$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementPresent('@MedicareProductLink',data.LoginWait)
                    .click('@MedicareProductLink')

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                browser.assert.urlContains('https://mercermarketplace.destinationrx.com/PlanCompare/Consumer/type1/2017/Compare/PinHome');
                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

            });
        }
    });

    this.When(/^User able to click and verify Dental Product Link for Non-Gated Portal$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementPresent('@DentalProductLink',data.LoginWait)
                    .click('@DentalProductLink')

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                browser.assert.urlContains('dental');
                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

            });
        }
    });

    this.When(/^User able to click and verify Vision Product Link for Non-Gated Portal$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementPresent('@VisionProductLink',data.LoginWait)
                    .click('@VisionProductLink')

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                browser.assert.urlContains('vision');
                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

            });
        }
    });



    //Regression Scenarios
    this.Given(/^User opens the Get Insured URL "([^"]*)" for regression$/, function (GI_URL) {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                console.log(GI_URL);
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(GI_URL);
                browser.timeoutsImplicitWait(30000);
                LandingPageGI.waitForElementVisible('@TollFreeNumberText',data.LoginWait)
                    .waitForElementVisible('@HomeLink',data.LoginWait)
                    .waitForElementVisible('@ContactUsLink',data.LoginWait)
                    .waitForElementVisible('@HelpfulLinks',data.LoginWait)
                    //  .waitForElementVisible('@SubsidyAccount',data.LoginWait)
                    .waitForElementVisible('@LogInLink',data.LoginWait)
                //  .waitForElementVisible('@SubsidyAccountLink',data.LoginWait);

            });
        }
    });

    this.When(/^User able to verify the subsidy account link$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                LandingPageGI.waitForElementVisible('@SubsidyAccountLink',data.LoginWait)
                    .click('@SubsidyAccountLink')

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.longWait)
                });

                SubsidyAccountPage.waitForElementVisible('@AccessReimbursementHeader',data.LoginWait)
                    .waitForElementVisible('@LoginTextbox',data.LoginWait)
                    .waitForElementVisible('@PasswordTextbox',data.LoginWait);
                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.longWait)
                });



            });
        }
    });

    this.When(/^User able to verify home link header$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                LandingPageGI.waitForElementVisible('@HomeLink',data.LoginWait)
                    .click('@HomeLink');
                LandingPageGI.waitForElementVisible('@TollFreeNumberText',data.LoginWait)
                    .waitForElementVisible('@HomeLink',data.LoginWait)
                    .waitForElementVisible('@ContactUsLink',data.LoginWait)
                    .waitForElementVisible('@HelpfulLinks',data.LoginWait)
                    .waitForElementVisible('@SubsidyAccountLink',data.LoginWait)
                    .waitForElementVisible('@LogInLink',data.LoginWait)
                    .waitForElementVisible('@SubsidyAccountLink',data.LoginWait);
            });
        }
    });

    this.When(/^User able to verify Helpful link$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {

                //Medical ID Link Verification
                LandingPageGI.waitForElementVisible('@HelpfulLinks',data.LoginWait)
                    .click('@HelpfulLinks')
                    .waitForElementVisible('@MedicaIDLink',data.LoginWait)
                    .click('@MedicaIDLink');

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                //MedicalIDPage.waitForElementVisible('@MedicalIDImg',data.LoginWait);
                browser.assert.urlContains('https://www.medicaid.gov/');
                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                LandingPageGI.waitForElementVisible('@ContactUsLink',data.LoginWait)
                    .waitForElementVisible('@HelpfulLinks',data.LoginWait);



                //Health Insurance Marketplace Verification
                LandingPageGI.waitForElementVisible('@HelpfulLinks',data.LoginWait)
                    .click('@HelpfulLinks')
                    .waitForElementVisible('@HIMLink',data.LoginWait)
                    .click('@HIMLink');

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                browser.assert.urlContains('//www.healthcare.gov/');
                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                LandingPageGI.waitForElementVisible('@ContactUsLink',data.LoginWait)
                    .waitForElementVisible('@HelpfulLinks',data.LoginWait);



                //Social Security Verification
                LandingPageGI.waitForElementVisible('@HelpfulLinks',data.LoginWait)
                    .click('@HelpfulLinks')
                    .waitForElementVisible('@SocialSecurityLink',data.LoginWait)
                    .click('@SocialSecurityLink');

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                browser.assert.urlContains('https://www.ssa.gov/');
                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                LandingPageGI.waitForElementVisible('@ContactUsLink',data.LoginWait)
                    .waitForElementVisible('@HelpfulLinks',data.LoginWait);



                // FAQ Page Verification
                LandingPageGI.waitForElementVisible('@HelpfulLinks',data.LoginWait)
                    .click('@HelpfulLinks')
                    .waitForElementVisible('@FAQsLink',data.LoginWait)
                    .click('@FAQsLink');

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                //HIMPage.waitForElementVisible('@HealthInsuranceHeader',data.LoginWait);
                browser.assert.urlContains('https://www.healthcare.gov/get-answers/');
                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                LandingPageGI.waitForElementVisible('@ContactUsLink',data.LoginWait)
                    .waitForElementVisible('@HelpfulLinks',data.LoginWait);




            });
        }
    });

    this.When(/^User able to verify the copyright year$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                LandingPageGI.waitForElementVisible('@CopyrightText',data.LoginWait)
                browser.useXpath().assert.containsText("//p[contains(text(),'Mercer LLC, All Rights Reserved')]",data.CopyrightYear)
            });
        }
    });

    this.When(/^User able to verify text on the homepage$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //  console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                LandingPageGI.waitForElementVisible('@Para1',data.LoginWait)
                    .waitForElementVisible('@Para2',data.LoginWait)
            });
        }
    });

    this.When(/^User able to verify the Here what to expect text at the footer$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                LandingPageGI.waitForElementVisible('@WhatToExpectText',data.LoginWait)
            });
        }
    });

    this.When(/^User able to click on the get started button$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                LandingPageGI.waitForElementVisible('@GetStartedButton',data.LoginWait)
                    .click('@GetStartedButton')
            });
        }
    });

    this.When(/^User Try login to the portal with wrong credentials and error msg is displayed having "([^"]*)"$/, function (TollFreeNumber) {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                LandingPageGI.waitForElementVisible('@FirstnameTextbox',data.LoginWait)
                    .waitForElementVisible('@LastnameTextbox',data.LoginWait)
                    .waitForElementVisible('@SSNTextbox',data.LoginWait)

                    //give wrong credentials:
                    .setValue('@FirstnameTextbox',data.WrongFirstName)
                    .setValue('@LastnameTextbox',data.WrongLastName)
                    .setValue('@SSNTextbox',data.WrongSSN)

                    .waitForElementVisible('@LoginContinueButton',data.LoginWait)
                    .click('@LoginContinueButton')

                browser.useXpath().assert.containsText("//div[contains(text(),'Invalid data. For assistance, call:')]","INVALID DATA. FOR ASSISTANCE, CALL: "+TollFreeNumber+"");


            });
        }
    });

    this.When(/^User enter correct credentials with "([^"]*)","([^"]*)"and"([^"]*)"to login successfully$/, function (FirstName,LastName,SSN) {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                LandingPageGI.waitForElementVisible('@FirstnameTextbox',data.LoginWait)
                    .clearValue('@FirstnameTextbox')
                    .waitForElementVisible('@LastnameTextbox',data.LoginWait)
                    .clearValue('@LastnameTextbox')
                    .waitForElementVisible('@SSNTextbox',data.LoginWait)
                    .clearValue('@SSNTextbox')

                    //give wrong credentials:
                    .setValue('@FirstnameTextbox',FirstName)
                    .setValue('@LastnameTextbox',LastName)
                    .setValue('@SSNTextbox',SSN)

                    .waitForElementVisible('@LoginContinueButton',data.LoginWait)
                    .click('@LoginContinueButton')
                    .waitForElementVisible('@ShopForHealthInsuranceHeader',data.LoginWait);
            });
        }
    });

    this.When(/^User verifies shop for health insurance page$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@HealthInsHeader',data.LoginWait)
            });
        }
    });

    this.When(/^User verify end date for Open enrollment$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@EndDateTab',data.LoginWait)
                browser.useXpath().assert.containsText('//h1[contains(text(),\' shop for Health Insurance\')]/span',data.EndDateOE);
            });
        }
    });

    this.When(/^User able to select any option on the page$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@LostCoverageLink',data.LoginWait)
                    .click('@LostCoverageLink')
                    .waitForElementVisible('@ZipcodeTextbox',data.LoginWait)
            });
        }
    });

    this.When(/^User able to enter household details and household income$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.getText("@IncomeYear",function(actual)
                {
                    var expected = "What's your projected 2018 household income?";
                    console.log(actual.value)
                    var prac = actual.value
                    if (prac === expected)
                    {
                        console.log("True")
                    }
                })
                ShopForHealthInsurancePage.waitForElementVisible('@ZipcodeTextbox',data.LoginWait)
                    .clearValue('@ZipcodeTextbox')
                    .setValue('@ZipcodeTextbox',data.Zipcode)
                    .waitForElementVisible('@MonthTextbox',data.LoginWait)
                    .clearValue('@MonthTextbox')
                    .setValue('@MonthTextbox',data.Month)
                    .waitForElementVisible('@DaysTextbox',data.LoginWait)
                    .clearValue('@DaysTextbox')
                    .setValue('@DaysTextbox',data.Day)
                    .waitForElementVisible('@YearTextbox',data.LoginWait)
                    .clearValue('@YearTextbox')
                    .setValue('@YearTextbox',data.Year)
                    .waitForElementVisible('@HouseholdIncomeTextbox',data.LoginWait)
                    .clearValue('@HouseholdIncomeTextbox')
                    .setValue('@HouseholdIncomeTextbox',data.HouseholdIncomeValue)
                    .waitForElementVisible('@SeeYourOptionsLink',data.LoginWait)
                    .click('@SeeYourOptionsLink');
            });
        }
    });

    this.Then(/^User Click on See Your Options to Lower Your Cost button and Compare option is displayed$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@CompareYourOptionTitle',data.LoginWait)
            });
        }
    });

    this.When(/^User clicks on the start shopping button$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                LandingPageGI.waitForElementVisible('@StartShoppingLoginUngatedPortal',data.LoginWait)
                    .click('@StartShoppingLoginUngatedPortal');
            });
        }
    });

    this.When(/^Health insurance page should be displayed$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                LandingPageGI.waitForElementVisible('@ShoppingHeaderUnGatedPortal',data.LoginWait);
            });
        }
    });

    this.When(/^User selects any option on the health insurance Page$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@LostCoverageLink',data.LoginWait)
                    .click('@LostCoverageLink');
            });
        }
    });

    this.When(/^Household page should be displayed$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@ZipcodeTextbox',data.LoginWait)
            });
        }
    });

    this.When(/^User enter the household details and its income$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        ShopForHealthInsurancePage.getText("@IncomeYear",function(actual)
        {
            var expected = "What's your projected 2018 household income?";
            console.log(actual.value)
            var prac = actual.value
            if (prac === expected)
            {
                console.log("True")
            }
        })
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@ZipcodeTextbox',data.LoginWait)
                    .clearValue('@ZipcodeTextbox')
                    .setValue('@ZipcodeTextbox',data.Zipcode)
                    .waitForElementVisible('@MonthTextbox',data.LoginWait)
                    .clearValue('@MonthTextbox')
                    .setValue('@MonthTextbox',data.Month)
                    .waitForElementVisible('@DaysTextbox',data.LoginWait)
                    .clearValue('@DaysTextbox')
                    .setValue('@DaysTextbox',data.Day)
                    .waitForElementVisible('@YearTextbox',data.LoginWait)
                    .clearValue('@YearTextbox')
                    .setValue('@YearTextbox',data.Year)
                    .waitForElementVisible('@HouseholdIncomeTextbox',data.LoginWait)
                    .clearValue('@HouseholdIncomeTextbox')
                    .setValue('@HouseholdIncomeTextbox',data.HouseholdIncomeValue)

                    .waitForElementVisible('@ContinueButton',data.LoginWait)
                    .click('@ContinueButton');
            });
        }
    });

    this.Then(/^User should be redirect to On Exchange flow$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                browser.pause(data.longWait)
                browser.assert.urlContains('exchangeType=ON');

            });
        }
    });

    this.When(/^User verifies the Health Product link$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@HealthProductLink',data.LoginWait)
                    .click('@HealthProductLink')
                    .waitForElementVisible('@LostCoverageLink',data.LoginWait);
            });
        }
    });

    this.When(/^User verifies the Medicare Product link$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                //Medicare Verification
                ShopForHealthInsurancePage.waitForElementVisible('@MedicareProductLink',data.LoginWait)
                    .click('@MedicareProductLink')

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.longWait)
                });
                browser.assert.urlContains(data.MedicareURL);
                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });


            });
        }
    });

    this.When(/^User verifies the dental product link$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                //Medicare Verification
                ShopForHealthInsurancePage.waitForElementVisible('@DentalProductLink',data.LoginWait)
                    .click('@DentalProductLink')

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.longWait)
                });
                browser.assert.urlContains(data.DentalURL);
                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });


            });
        }
    });

    this.When(/^User verifies the Vision product link$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                //Medicare Verification
                ShopForHealthInsurancePage.waitForElementVisible('@DentalProductLink',data.LoginWait)
                    .click('@DentalProductLink')

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.longWait)
                });
                browser.assert.urlContains(data.VisionURL);
                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });


            });
        }
    });

    this.When(/^User verifies the other insurance product link appear only for "([^"]*)" Motorola Client$/, function (URL) {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                //Motorola other insurance Verification
                if(URL.includes('Motorolasolutions'))
                {
                    ShopForHealthInsurancePage.waitForElementVisible('@OtherInsuranceLink',data.LoginWait)
                        .click('@OtherInsuranceLink')

                    browser.pause(data.shortWait)
                    browser.windowHandles(function(result) {
                        var handle = result.value[1];
                        browser.switchWindow(handle);
                        browser.maximizeWindow();
                        browser.pause(data.longWait)
                    });
                    browser.assert.urlContains(data.OtherInsuranceURL);
                    browser.closeWindow();

                    browser.pause(data.shortWait)
                    browser.windowHandles(function(result) {
                        var handle = result.value[0];
                        browser.switchWindow(handle);
                        browser.maximizeWindow();
                        browser.pause(data.shortWait)
                    });
                }
            });
        }
    });

    this.Then(/^User enter wrong zipcode and error msg should be displayed$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@ZipcodeTextbox',data.LoginWait)
                    .clearValue('@ZipcodeTextbox')
                    .setValue('@ZipcodeTextbox',data.WrongZipcode)
                    .click('@HouseholdIncomeTextbox')
                    .waitForElementVisible('@EnterValidZipcodeError',data.LoginWait)
                    .clearValue('@ZipcodeTextbox')
                    .setValue('@ZipcodeTextbox',data.Zipcode)
            });
        }
    });

    this.Then(/^User make new household members with wrong birthdate and should be accepted$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@ZipcodeTextbox',data.LoginWait)
                    .waitForElementVisible('@AddSpouseLink',data.LoginWait)
                    .click('@AddSpouseLink')
                    .waitForElementVisible('@SpouseDOBMonth',data.LoginWait)
                    .waitForElementVisible('@SpouseDOBDay',data.LoginWait)
                    .waitForElementVisible('@SpouseDOBYear',data.LoginWait)
                    .setValue('@SpouseDOBMonth',data.WrongMonthSpouse)
                    .setValue('@SpouseDOBDay',data.WrongDaySpouse)
                    .setValue('@SpouseDOBYear',data.WrongYearSpouse)
            });
        }
    });

    this.Then(/^User clicks on See Your Options and error msg should be displayed$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@SeeYourOptionsLink',data.LoginWait)
                    .click('@SeeYourOptionsLink')
                    .waitForElementVisible('@ErrorMsgInvalidDOB',data.LoginWait)
            });
        }
    });

    this.Then(/^User clicks on Continue and error msg should be displayed$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@ContinueButton',data.LoginWait)
                    .click('@ContinueButton')
                    .waitForElementVisible('@ErrorMsgInvalidDOB',data.LoginWait)
            });
        }
    });

    this.Then(/^User try to add children below 26 years age and should be added$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@AddChildLink',data.LoginWait)
                    .click('@AddChildLink')
                    .waitForElementVisible('@Child1DOBMonth',data.LoginWait)
                    .waitForElementVisible('@Child1DOBDay',data.LoginWait)
                    .waitForElementVisible('@Child1DOBYear',data.LoginWait)
                    .setValue('@Child1DOBMonth',data.Child1Month)
                    .setValue('@Child1DOBDay',data.Child1Day)
                    .setValue('@Child1DOBYear',data.Child1Year)
                    .click('@HouseholdIncomeTextbox');
            });
        }
    });

    this.Then(/^Tobacco checkbox should be present and be checked$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@TobaccoUseCheckboxApplicant',data.LoginWait)
                ShopForHealthInsurancePage.waitForElementVisible('@TobaccoUseCheckboxSpouse',data.LoginWait)
                ShopForHealthInsurancePage.waitForElementVisible('@TobaccoUseCheckboxChild1',data.LoginWait)
            });
        }
    });

    this.Then(/^User able to check seeking Coverage checkbox should be checked$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@SeekingCoverageCheckboxChild1',data.LoginWait)
                ShopForHealthInsurancePage.waitForElementVisible('@SeekingCoverageCheckboxSpouse',data.LoginWait)
                ShopForHealthInsurancePage.waitForElementVisible('@SeekingCoverageCheckboxApplicant',data.LoginWait)
            });
        }
    });

    this.Then(/^User able to see pregnant checkbox should be present$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@PregnantCheckboxApplicant',data.LoginWait)
                ShopForHealthInsurancePage.waitForElementVisible('@PregnantCheckboxSpouse',data.LoginWait)
                ShopForHealthInsurancePage.waitForElementVisible('@PregnantCheckboxChild1',data.LoginWait)
            });
        }
    });

    this.Then(/^User clicks on See Your Options and compare your option page should be displayed$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@SeeYourOptionsLink',data.LoginWait)
                    .click('@SeeYourOptionsLink')
                    .waitForElementVisible('@CompareYourOptionTitle',data.LoginWait)
            });
        }
    });

    this.Then(/^User verifies the HRA decision Page$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@HowYouEmployerContributionText',data.LoginWait)
                browser.useXpath().assert.containsText("//strong[contains(text(),'How your employer contribution works:')]/../../p[2]",data.ContributionParagraph);
            });
        }
    });

    this.Then(/^User verifies Decision Page Footer$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                browser.useXpath().assert.containsText("//div[@class='disclaimerRoot disclaimer-box']/p[2]/strong",data.HRADisclaimer);
                browser.useXpath().assert.containsText("//div[@class='disclaimerRoot disclaimer-box']/p[3]/strong",data.TaxCreditDisclaimer);
            });
        }
    });

    this.Then(/^User clicks on Shop with employer contribution and navigate to OFF Exchange Flow$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@ShopWithEmpContButton',data.LoginWait)
                    .click('@ShopWithEmpContButton')
                    .waitForElementVisible('@CloseButtonPopup',data.LoginWait)
                browser.assert.urlContains(data.OffExchangeURL);
            });
        }
    });

    this.Then(/^Personalized plan score page should be displayed$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@CloseButtonPopup',data.LoginWait)
            });
        }
    });

    this.Then(/^User close the Personalised plan score and clicks on Learn More in Health Plan Page$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@CloseButtonPopup',data.LoginWait)
                    .click('@CloseButtonPopup')
                    .waitForElementVisible('@LearnMoreTooltip',data.LoginWait)
                    .click('@LearnMoreTooltip')
            });
        }
    });

    this.Then(/^User able to verify the tooltip text for Learn More Link$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@LearnMoreTooltip',data.LoginWait)
            });
        }
    });

    this.Then(/^User clicks on Shop with tax credits and personalized page is displayed$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@ShopWithTaxCreditsButton',data.LoginWait)
                    .click('@ShopWithTaxCreditsButton')

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.longWait)
                });
                browser.assert.urlContains('exchangeType=ON#1');

                ShopForHealthInsurancePage
                //.waitForElementVisible('@PersonalizedPlanScoresTab',data.LoginWait)
                //.waitForElementVisible('@BackToHRAAndCreditLink',data.LoginWait)
                    .waitForElementVisible('@CloseButtonPopup',data.LoginWait)
                    .click('@CloseButtonPopup')
                browser.pause(10000);
                //  browser.closeWindow();

            });
        }
    });

    this.Then(/^User should select three plans for comparision$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {

                browser.useXpath().click("(//label[contains(text(),'Compare')])["+3+"]");
                browser.pause(data.Microwait);
                browser.useXpath().click("(//label[contains(text(),'Compare')])["+1+"]");
                browser.pause(data.Microwait);
                browser.useXpath().click("//a[contains(.,'Hide Compare')]")
                browser.pause(data.Microwait);
                browser.useXpath().click("(//label[contains(text(),'Compare')])["+2+"]");
                browser.pause(data.Microwait);
                browser.useXpath().click("//a[contains(.,'Show Compare')]")
                browser.pause(data.Microwait);
            });
        }
    });

    this.Then(/^User clicks on Compare plan and comparision page should be displayed$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@CompareNowLink',data.LoginWait)
                    .click('@CompareNowLink')
                    .waitForElementVisible('@ComparisionColoumn',data.LoginWait)
            });
        }
    });

    this.Then(/^User verifies Verify three plans header is displayed$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@ThreePlanHeader',data.LoginWait)
            });
        }
    });

    this.Then(/^User clicks on back to all plans and page should be displayed$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@BackToAllPlanLink',data.LoginWait)
                    .click('@BackToAllPlanLink')
                    .waitForElementVisible('@CompareNowLink',data.LoginWait)
            });
        }
    });

    this.Then(/^User clicks on view details for one plan and can see it successfully$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@FirstPlanDetails',data.LoginWait)
                    .click('@FirstPlanDetails')
                    .waitForElementVisible('@PlanHighlightsTitle',data.LoginWait)
            });
        }
    });

    this.Then(/^User opens the personalised plan scores page$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@BackLink2',data.LoginWait)
                    .click('@BackLink2')
                browser.pause(data.Microwait)
                browser.useXpath().click("//a[contains(.,'Hide Compare')]")
                browser.pause(data.Microwait)
                ShopForHealthInsurancePage.waitForElementVisible('@PersonalizedPlanScoresTab',data.LoginWait)
                    .click('@PersonalizedPlanScoresTab')
                    .waitForElementVisible('@CloseButtonPopup',data.LoginWait)
            });
        }
    });

    this.Then(/^User types a doctors name and it should get selected$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@DoctorNameSearchTextbox',data.LoginWait)
                    .clearValue('@DoctorNameSearchTextbox')
                    .setValue('@DoctorNameSearchTextbox',data.DoctorsName);
                browser.pause(data.shortWait);
                //  browser.useXpath().click("//p[contains(text(),'"+data.DoctorsName+"')]/../..");
                browser.useXpath().click("(//p[contains(.,'"+data.DoctorsName+"')])[1]/../..");
                browser.pause(data.shortWait);


                // ShopForHealthInsurancePage.waitForElementVisible('@JareThronDropdown',data.LoginWait)
                //     .click('@JareThronDropdown')
                //     .waitForElementVisible('@SelectedDoctor',data.LoginWait)

            });
        }
    });

    this.Then(/^User can select drug with dosage successfully$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@AddDrugTextbox',data.LoginWait)
                    .clearValue('@AddDrugTextbox')
                    .setValue('@AddDrugTextbox',data.DrugName);
                browser.pause(data.shortWait);
                ShopForHealthInsurancePage.waitForElementVisible('@DrugDropdown',data.LoginWait)
                    .click('@DrugDropdown')
                    .waitForElementVisible('@DrugDoseInput',data.LoginWait)
                    .click('@DrugDoseInput')
                    .waitForElementVisible('@DoseInputNext',data.LoginWait)
                    .click('@DoseInputNext')
                    .waitForElementVisible('@DoseInputAdd',data.LoginWait)
                //.click('@DoseInputAdd')
                //.waitForElementVisible('@RemovePrismol',data.LoginWait)
                //.click('@RemovePrismol')
                //.waitForElementNotVisible('@RemovePrismol',data.LoginWait);
            });
        }
    });

    this.Then(/^User can able to select optional benefit and it should be checked$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@OptionalBenefits',data.LoginWait)
            });
        }
    });

    this.Then(/^User clicks on Personalised plan score and it should be displayed$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@PersonalisedPlanScoreCalc',data.LoginWait)
                    .click('@PersonalisedPlanScoreCalc')
            });
        }
    });

    this.Then(/^User fills household data and click on the continue Button$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@ZipcodeTextbox',data.LoginWait)
                    .clearValue('@ZipcodeTextbox')
                    .setValue('@ZipcodeTextbox',data.Zipcode)
                    .waitForElementVisible('@ApplicantDOBMonth',data.LoginWait)
                    .waitForElementVisible('@ApplicantDOBDay',data.LoginWait)
                    .waitForElementVisible('@ApplicantDOBYear',data.LoginWait)
                    .setValue('@ApplicantDOBMonth',data.Month)
                    .setValue('@ApplicantDOBDay',data.Day)
                    .setValue('@ApplicantDOBYear',data.Year)
                ShopForHealthInsurancePage.waitForElementVisible('@ContinueButton',data.LoginWait)
                    .click('@ContinueButton')

            });
        }
    });

    this.Then(/^User able to open Personalised Plan Page$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {

                ShopForHealthInsurancePage
                //.waitForElementVisible('@PersonalizedPlanScoresTab',data.LoginWait)
                //.waitForElementVisible('@BackToHRAAndCreditLink',data.LoginWait)
                    .waitForElementVisible('@CloseButtonPopup',data.LoginWait)
                    .click('@CloseButtonPopup')
                browser.pause(10000);
                //  browser.closeWindow();

            });
        }
    });

    this.Then(/^User clicks on Personalised plan score and it should be displayed for GatedPortal$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementVisible('@PersonalisedPlanScoreCalc',data.LoginWait)
                    .click('@PersonalisedPlanScoreCalc')

                browser.closeWindow();
                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.longWait)
                });
            });
        }
    });



}

