Feature: Get Insured Regression Test cases

  @RegressionGI @VerifyHomePage @SmokeGI
  Scenario Outline: To Verify homepage for gated and ungated portal
    Given User opens the Get Insured URL "<GetInsuredURL>" for regression
    When User able to verify the tollfree number "<TollFreeNumber>"
    Then  User able to verify the logo of Mercer Marketplace


    Examples:
      | GetInsuredURL                                                |TollFreeNumber |
      |https://RetireeHealth.premedicareplans.com/subsidy            |866-609-4810   |
      |https://Ahlstrom.premedicareplans.com/subsidy                 |844-618-6285   |
