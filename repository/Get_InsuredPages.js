/**
 * Created by subhajit-chakraborty on 7/6/2017.
 */

module.exports = {
    sections: {
        LandingPage: {
            selector: 'body',
            elements: {
                //QAF
                MercerImgLink: {locateStrategy: 'xpath',selector: "//div[@class='hidden-xs']//img[@alt='Getinsured.com']"},
                GetStartedButton:  {locateStrategy: 'xpath',selector: "//a[contains(text(),'GET STARTED')]"},
                TollFreeNumberText: {locateStrategy: 'xpath',selector: "//a[@id='ClickTrackIVRNumberNull']/../a[@id='ClickTrackNumberNotNull']"},
                HomeLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Home')]"},
                ContactUsLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Contact Us')]"},
                HelpfulLinks: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Helpful Links')]"},
                SubsidyAccount: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Subsidy Account')]"},
                SignUpLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Sign Up')]"},
                LogInLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Log In')]"},
                SubsidyAccountLink: {locateStrategy: 'xpath',selector: "//ul[@id='primary-navbar']/li[2]/ul/li[4]/a"},

                MedicaIDLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Medicaid')]"},
                HIMLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Health Insurance Marketplace')]"},
                SocialSecurityLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Social Security')]"},
                FAQsLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'FAQs')]"},

                GetStartedLoginGatedPortal: {locateStrategy: 'xpath',selector: "//a[contains(.,'GET STARTED')]"},
                FirstnameTextbox: {selector: "#firstname"},
                LastnameTextbox: {selector: "#lastname"},
                SSNTextbox: {selector: "#ssn"},
                LoginContinueButton: {locateStrategy: 'xpath',selector: "//button[contains(text(),'Continue')]"},
                ShopForHealthInsuranceHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'shop for Health Insurance')]"},

                StartShoppingLoginUngatedPortal: {locateStrategy: 'xpath',selector: "//a[contains(.,'Shopping')]/span[contains(text(),'Start')]"},
                ShoppingHeaderUnGatedPortal: {locateStrategy: 'xpath',selector: "//h1[contains(text(),' shop for Health Insurance')]"},

                //Footer
                FooterParaLink1: {selector: "p[footer-disclaimer-links='0']"},
                FooterParaLink2: {selector: "p[footer-disclaimer-links='1']"},

                PrivacyPolicyLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Privacy Notice')]"},
                TermsAndConditionLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Terms & Conditions')]"},

                CopyrightText: {locateStrategy: 'xpath',selector: "//p[contains(text(),'Mercer LLC, All Rights Reserved')]"},

                Para1: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'focused on helping you find the health coverage you need')]"},
                Para2: {locateStrategy: 'xpath',selector: "//p[contains(text(),'We have the right solutions and trusted experience to help you understand your insurance choices and assist you in making the best decision for you and your family.')]"},
                WhatToExpectText: {locateStrategy: 'xpath',selector: '//h2[contains(text(),"Here\'s What to Expect")]'},


            }
        },

        ContactUsPage: {
            selector: 'body',
            elements: {
                //QAF
                ContactUsMarketplaceHeader:  {locateStrategy: 'xpath',selector: "//h1/strong[contains(text(),'Contact Mercer Marketplace')]"},
            }
        },

        MedicalIDPage: {
            selector: 'body',
            elements: {
                //QAF
                MedicalIDImg:  {selector: "a[title='Medicaid.gov Home'] img"},
            }
        },

        HIMPage: {
            selector: 'body',
            elements: {
                //QAF
                HealthInsuranceHeader:  {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Need health insurance?')]"},
            }
        },

        SocialSecurityPage: {
            selector: 'body',
            elements: {
                //QAF
                SocialSecurityHeader:  {locateStrategy: 'xpath',selector: "///header[@id='banner']//a[contains(text(),'Social Security')]"},
            }
        },

        FAQPage: {
            selector: 'body',
            elements: {
                //QAF
                FAQHeader:  {locateStrategy: 'xpath',selector: "//h1[contains(text(),'How can we help you?')]"},
            }
        },

        ShopForHealthInsurancePage: {
            selector: 'body',
            elements: {
                //QAF
                HealthInsHeader:  {locateStrategy: 'xpath',selector: "//h1[contains(text(),' shop for Health Insurance')]"},
                LostCoverageLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Lost coverage')]"},
                SeeYourOptionsLink: {locateStrategy: 'xpath',selector: "//a[@id='eligibilityNext'][contains(text(),'See Your Options')]"},
                CompareYourOptionTitle: {locateStrategy: 'xpath',selector: "//h2[contains(text(),'Compare your Options')]"},
                ShopWithEmpContButton: {locateStrategy: 'xpath',selector: "//a[contains(text(),'SHOP WITH EMPLOYER CONTRIBUTION')]"},
                ShopWithTaxCreditsButton: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Shop with tax credits')]"},
                HomeButton: {locateStrategy: 'xpath',selector: "(//a[contains(text(),'Home')])[1]"},
                ZipcodeTextbox: {selector: "#zipcode"},
                MonthTextbox: {locateStrategy: 'xpath',selector: "(//input[@type='tel'])[2]"},
                DaysTextbox: {locateStrategy: 'xpath',selector: "(//input[@type='tel'])[3]"},
                YearTextbox: {locateStrategy: 'xpath',selector: "(//input[@type='tel'])[4]"},
                IncomeYear: {locateStrategy: 'xpath',selector: "(//label[@class='link-style__tooltip ng-binding'])[2]"},

                SeePlansLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'See plans')]"},
                CheckForTaxCreditsButton: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Check for tax credits')]"},
                HouseholdIncomeTextbox: {selector: "#income"},
                CheckForTaxCreditsButtonNonGated: {locateStrategy: 'xpath',selector: "//a[contains(.,'for tax credits')]"},
                //ShopWithTaxCreditsButton: {selector: "//a[contains(text(),'Shop with tax credits')]"},

                HealthProductLink: {locateStrategy: 'xpath',selector : "//li[@id='isAffiliateHealthEnabled']/a"},
                MedicareProductLink: {locateStrategy: 'xpath',selector: "//li[@id='isAffiliateMedicareEnabled']/a"},
                DentalProductLink: {locateStrategy: 'xpath',selector: "//li[@id='isAffiliateDENTALEnabled']/a"},
                VisionProductLink: {locateStrategy: 'xpath',selector: "//li[@id='isAffiliateVISIONEnabled']/a"},
                OtherInsuranceLink: {locateStrategy: 'xpath',selector: "//li[@id='isAffiliateAMEEnabled']/a"},

                EndDateTab: {locateStrategy: 'xpath',selector: "//h1[contains(text(),' shop for Health Insurance')]/span"},
                ContinueButton: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Continue')]"},

                EnterValidZipcodeError: {locateStrategy: 'xpath',selector: "//div[contains(text(),'Please enter a valid zipcode.')]"},
                AddSpouseLink: {locateStrategy: 'xpath',selector: "//a[contains(.,'Add Spouse')]"},
                SpouseDOBMonth: {locateStrategy: 'xpath',selector: "//span[contains(text(),'Spouse')]/../../../div[3]/div/input[1]"},
                SpouseDOBDay: {locateStrategy: 'xpath',selector: "//span[contains(text(),'Spouse')]/../../../div[3]/div/input[2]"},
                SpouseDOBYear: {locateStrategy: 'xpath',selector: "//span[contains(text(),'Spouse')]/../../../div[3]/div/input[3]"},
                ApplicantDOBMonth: {locateStrategy: 'xpath',selector: "//span[contains(text(),'Applicant')]/../../../div[3]/div/input[1]"},
                ApplicantDOBDay: {locateStrategy: 'xpath',selector: "//span[contains(text(),'Applicant')]/../../../div[3]/div/input[2]"},
                ApplicantDOBYear: {locateStrategy: 'xpath',selector: "//span[contains(text(),'Applicant')]/../../../div[3]/div/input[3]"},

                ErrorMsgInvalidDOB: {locateStrategy: 'xpath',selector: "//ul[@class='list-unstyled member-ul']/li[1]"},
                AddChildLink: {locateStrategy: 'xpath',selector: "//a[contains(.,'Add Child')]"},
                Child1DOBMonth: {locateStrategy: 'xpath',selector: "//span[contains(text(),'Child 1')]/../../../div[3]/div/input[1]"},
                Child1DOBDay: {locateStrategy: 'xpath',selector: "//span[contains(text(),'Child 1')]/../../../div[3]/div/input[2]"},
                Child1DOBYear: {locateStrategy: 'xpath',selector: "//span[contains(text(),'Child 1')]/../../../div[3]/div/input[3]"},

                TobaccoUseCheckboxApplicant: {locateStrategy: 'xpath',selector: "//span[contains(text(),'Applicant')]/../../..//input[@ng-model='member.isTobaccoUser']/../span"},
                TobaccoUseCheckboxSpouse: {locateStrategy: 'xpath',selector: "//span[contains(text(),'Spouse')]/../../..//input[@ng-model='member.isTobaccoUser']/../span"},
                TobaccoUseCheckboxChild1: {locateStrategy: 'xpath',selector: "//span[contains(text(),'Child 1')]/../../..//input[@ng-model='member.isTobaccoUser']/../span"},
                SeekingCoverageCheckboxApplicant: {locateStrategy: 'xpath',selector: "//span[contains(text(),'Applicant')]/../../..//input[@ng-model='member.isSeekingCoverage']/../span"},
                SeekingCoverageCheckboxSpouse: {locateStrategy: 'xpath',selector: "//span[contains(text(),'Spouse')]/../../..//input[@ng-model='member.isSeekingCoverage']/../span"},
                SeekingCoverageCheckboxChild1: {locateStrategy: 'xpath',selector: "//span[contains(text(),'Child 1')]/../../..//input[@ng-model='member.isSeekingCoverage']/../span"},
                PregnantCheckboxApplicant: {locateStrategy: 'xpath',selector :"//span[contains(text(),'Applicant')]/../../..//input[@ng-model='member.isPregnant']/../span"},
                PregnantCheckboxSpouse: {locateStrategy: 'xpath',selector :"//span[contains(text(),'Spouse')]/../../..//input[@ng-model='member.isPregnant']/../span"},
                PregnantCheckboxChild1: {locateStrategy: 'xpath',selector :"//span[contains(text(),'Child 1')]/../../..//input[@ng-model='member.isPregnant']/../span"},

                ChildTableRemove: {locateStrategy: 'xpath',selector: "//span[contains(text(),'Child 1')]/../../..//i"},
                SpouseTableRemove: {locateStrategy: 'xpath',selector: "//span[contains(text(),'Spouse')]/../../..//i"},

                HowYouEmployerContributionText: {locateStrategy: 'xpath',selector: "//strong[contains(text(),'How your employer contribution works:')]"},
                //ContributionPara: {selector: ""}
                CloseButtonPopup: {locateStrategy: 'xpath',selector: "//h4[contains(text(),'Find the right plan by answering a few quick questions')]/../button"},
                LearnMoreLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),' Learn More')]"},
                LearnMoreTooltip: {locateStrategy: 'xpath',selector: "//span[contains(text(),'Health Plans')]/../../div[1]//a[@rel='tooltip'][@data-original-title='Don’t forget you have a monthly subsidy from your employer for your monthly premiums & other eligible out-of-pocket expenses. The subsidy amounts shown do not reflect any amount tied to a Medicare-eligible participant. Have questions about your Subsidy? Please visit the Subsidy Account page by clicking on SUBSIDY ACCOUNT in the top header section. This is where you will find more information on how to use your employer subsidy, how to file for a reimbursement, how to check your balance, and many other employer subsidy related topics.']"},

                PersonalizedPlanScoresTab: {locateStrategy: 'xpath',selector: "(//a[contains(text(),'Personalize plan scores')])[1]"},
                BackToHRAAndCreditLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Back to HRA & Tax Credit')]"},
                CompareNowLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Compare Now')]"},
                ComparisionColoumn: {selector: "#mainSummaryCmp"},
                ThreePlanHeader: {locateStrategy: 'xpath',selector: "//span[contains(text(),'Health Plans')]/../span[contains(text(),'3')]"},
                BackToAllPlanLink: {selector :"a[class='backToAll tile__back-btn'] i"},
                BackLink2: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Back to All Plans')]"},
                FirstPlanDetails: {locateStrategy: 'xpath',selector: "(//a[contains(text(),'Details')])[1]"},
                PlanHighlightsTitle: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Plan Highlights')]"},

                DoctorNameSearchTextbox: {locateStrategy: 'xpath',selector: "//input[@id='medical']"},
                JareThronDropdown: {locateStrategy: 'xpath',selector: "//span[contains(text(),'Specialist, Chicago, IL')]/../../.."},
                SelectedDoctor: {locateStrategy: 'xpath',selector: "//ul[@id='myMedicalProviderListUl']/li/span[contains(text(),'JA\'RE THORN')]"},
                AddDrugTextbox: {locateStrategy: 'xpath',selector: "//input[@class='js-typeahead-drugsearch']"},
                DrugDropdown: {locateStrategy: 'xpath',selector: "//strong[contains(text(),'Primsol')]"},
                DrugDoseInput: {locateStrategy: 'xpath',selector: "//fieldset[@id='dosageSelection']/div/input"},
                DoseInputNext: {locateStrategy: 'xpath',selector: "//button[@id='dosage-next']"},
                DoseInputAdd: {locateStrategy: 'xpath',selector: "//button[@id='dosage-submit']"},
                RemovePrismol: {locateStrategy: 'xpath',selector: "//div[@id='Primsol']/div/i"},
                OptionalBenefits: {locateStrategy: 'xpath',selector: "//legend[contains(text(),'Are any of these')]"},
                PersonalisedPlanScoreCalc: {locateStrategy: 'xpath',selector: "(//a[contains(text(),'Personalize plan scores')])[2]"},














            }
        },

        SubsidyAccountPage: {
            selector: 'body',
            elements: {
                //QAF
                AccessReimbursementHeader:  {locateStrategy: 'xpath',selector: "//h6[contains(text(),'Access Your Reimbursement Account')]"},
                LoginTextbox: {selector: "#login_id"},
                PasswordTextbox: {selector: "#password"},


            }
        },
    }
};
